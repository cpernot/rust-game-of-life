mod game;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "Conway", about = "Conway's Game Of Life in Rust")]
struct Config {
    #[structopt(short, long, default_value = "20")]
    fps: u64,
    #[structopt(short, long, default_value = "#")]
    active: char,
}

fn main() {
    let conf = Config::from_args();
    let mut g = game::Game::new(conf.fps, conf.active);
    g.mainloop();
}
