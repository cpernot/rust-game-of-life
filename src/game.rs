use rand::Rng;
use std::{
    io::{stdout, Write},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::{Duration, Instant},
};
use termion::screen::AlternateScreen;
#[derive(Clone, Debug)]
pub struct Game {
    board: Vec<bool>,
    width: usize,
    height: usize,
    fps: u64,
    active: char,
}

impl Game {
    pub fn new(fps: u64, active: char) -> Self {
        let mut rng = rand::thread_rng();
        let (rows, col) = termion::terminal_size().unwrap_or((10, 10));
        let (width, height) = (rows as usize, col as usize);
        let board = (0..width * height).map(|_| rng.gen_bool(0.4)).collect();
        Self {
            board,
            width,
            height,
            fps,
            active,
        }
    }
    pub fn get(&self, x: i32, y: i32) -> bool {
        if x < 0 || y < 0 || x >= self.width as i32 || y >= self.height as i32 {
            false
        } else {
            self.board[y as usize * self.width + x as usize]
        }
    }
    fn alive_neigh(&self, x: usize, y: usize) -> usize {
        (0..9)
            .map(|n| (n % 3 - 1, n / 3 - 1))
            .filter(|&(rel_x, rel_y)| rel_x != 0 || rel_y != 0)
            .filter(|&(rel_x, rel_y)| self.get(x as i32 + rel_x, y as i32 + rel_y))
            .count()
    }
    pub fn advance(&mut self) {
        self.board = (0..self.height * self.width)
            .map(|n| (n % self.width, n / self.width))
            .map(
                |(x, y)| match (self.get(x as i32, y as i32), self.alive_neigh(x, y)) {
                    (true, 2 | 3) => true,
                    (false, 3) => true,
                    _otherwise => false,
                },
            )
            .collect()
    }
    pub fn render<W: Write>(&self, screen: &mut AlternateScreen<W>) {
        write!(
            screen,
            "{}{}",
            termion::cursor::Goto(1, 1),
            self.board
                .iter()
                .map(|&b| if b { self.active } else { ' ' })
                .collect::<String>()
        )
        .unwrap();
    }
    pub fn mainloop(&mut self) {
        let mut screen = AlternateScreen::from(stdout());
        let running = Arc::new(AtomicBool::new(true));
        let r = running.clone();
        ctrlc::set_handler(move || {
            r.store(false, Ordering::SeqCst);
        })
        .unwrap();
        while running.load(Ordering::SeqCst) {
            let timer = Instant::now();
            self.render(&mut screen);
            self.advance();
            let remaining = Duration::from_millis(1000 / self.fps).saturating_sub(timer.elapsed());
            if !remaining.is_zero() {
                std::thread::sleep(remaining);
            }
        }
    }
}
